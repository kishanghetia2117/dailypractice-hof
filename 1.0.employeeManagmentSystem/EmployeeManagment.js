// 1. Find all Web Developers
const webDev = (employeeDb) => {
    return employeeDb.filter(employee => employee.job.includes("Web Developer"));
}
// 2.Convert all the salary values into proper numbers instead of strings 
const salaryToNum = (employeeDb) => {
    return employeeDb.map((employee) => {
        let newobj = {...employee}
        newobj.salary = parseFloat(newobj.salary.slice(1));
        return newobj;
    });

}
// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
const factorOfSalary = (employeeDb) => {
    return employeeDb.map((employee) => {
        let newobj = {...employee}
        newobj.factoredSalary = parseFloat(newobj.salary.slice(1)) * 10000;
        return newobj;
    });
}
// 4. Find the sum of all salaries 
const grossSalary = (employeeDb) => {
    return employeeDb.reduce((salarySum, employee) => {
        return salarySum += employee.factoredSalary;
    }, 0)
}
// 5. Find the sum of all salaries based on country using only HOF method
const grossSalaryOfCountries = (employeeDb) => {
    return employeeDb.reduce((countryGrossSalary, employee) => {
        if (countryGrossSalary.hasOwnProperty(employee.location)) {
            countryGrossSalary[employee.location] += employee.factoredSalary;
        } else {
            countryGrossSalary[employee.location] = employee.factoredSalary;
        }
        return countryGrossSalary
    }, {})
}
// 6. Find the average salary of based on country using only HOF method
const averageSalaryOfCountries = (employeeDb) => {
    const grossAndCount = employeeDb.reduce((countryGrossSalaryAndCOunt, employee) => {
        if (countryGrossSalaryAndCOunt.hasOwnProperty(employee.location)) {
            let salary = countryGrossSalaryAndCOunt[employee.location].salary += employee.factoredSalary;
            let count = countryGrossSalaryAndCOunt[employee.location].count += 1;
            countryGrossSalaryAndCOunt[employee.location]['Avg'] += salary/count
        } else {
            countryGrossSalaryAndCOunt[employee.location] = {}
            let salary = countryGrossSalaryAndCOunt[employee.location].salary = employee.factoredSalary;
            let count = countryGrossSalaryAndCOunt[employee.location].count = 1;
            countryGrossSalaryAndCOunt[employee.location]['Avg'] = salary/count

        }
        return countryGrossSalaryAndCOunt
    }, {})
    return grossAndCount
}

module.exports = {
    webDev,
    salaryToNum,
    factorOfSalary,
    grossSalary,
    grossSalaryOfCountries,
    averageSalaryOfCountries
}