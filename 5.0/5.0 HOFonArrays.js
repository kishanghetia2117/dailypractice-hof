// =============================================== ForEach, Map, Filter ========================================================

/*
Write a function called doubleValues which accepts an array and returns a new array with all the values in the array passed to the function doubled

Examples:
    doubleValues([1,2,3]) // [2,4,6]
    doubleValues([5,1,2,3,10]) // [10,2,4,6,20]

*/
function doubleValues(arr) {
    return arr.map((arrEle) => arrEle * 2);
}
console.log("----------------------------doubleValues---------------------------");
console.log(doubleValues([1, 2, 3]));
console.log(doubleValues([5, 1, 2, 3, 10]));
/*
Write a function called onlyEvenValues which accepts an array and returns a new array with only the even values in the array passed to the function

Examples:
    onlyEvenValues([1,2,3]) // [2]
    onlyEvenValues([5,1,2,3,10]) // [2,10]

*/
function onlyEvenValues(arr) {
    return arr.filter((arrEle) => arrEle % 2 === 0);
}
console.log("----------------------------onlyEvenValues---------------------------");
console.log(onlyEvenValues([1, 2, 3]));
console.log(onlyEvenValues([5, 1, 2, 3, 10]));
/*
Write a function called showFirstAndLast which accepts an array of strings and returns a new array with only the first and last character of each string.

Examples:
    showFirstAndLast(['colt','matt', 'tim', 'test']) // ["ct", "mt", "tm", "tt"]
    showFirstAndLast(['hi', 'goodbye', 'smile']) // ['hi', 'ge', 'se']

*/
function showFirstAndLast(arr) {
    return arr.map((arrEle) => arrEle.at(0) + arrEle.at(-1));
}
console.log("----------------------------showFirstAndLast---------------------------");
console.log(showFirstAndLast(['colt', 'matt', 'tim', 'test']));
console.log(showFirstAndLast(['hi', 'goodbye', 'smile']));

/*
Write a function called addKeyAndValue which accepts an array of objects, a key, and a value and returns the array passed to the function with the new key and value added for each object 

Examples:
    addKeyAndValue([{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}, {name: 'Colt'}], 'title', 'instructor') 
    
    // [{name: 'Elie', title:'instructor'}, {name: 'Tim', title:'instructor'}, {name: 'Matt', title:'instructor'}, {name: 'Colt', title:'instructor'}]

*/
function addKeyAndValue(arr, key, value) {
    arr.forEach((arrEle) => arrEle.key = value);
    return arr;
}
console.log("----------------------------addKeyAndValue---------------------------");
let array = [{ name: 'Elie' }, { name: 'Tim' }, { name: 'Matt' }, { name: 'Colt' }];
console.log(addKeyAndValue(array, 'title', 'instructor'));

/*
Write a function called vowelCount which accepts a string and returns an object with the keys as the vowel and the values as the number of times the vowel appears in the string. This function should be case insensitive so a lowercase letter and uppercase letter should count

Examples:
    vowelCount('Elie') // {e:2,i:1};
    vowelCount('Tim') // {i:1};
    vowelCount('Matt') // {a:1})
    vowelCount('hmmm') // {};
    vowelCount('I Am awesome and so are you') // {i: 1, a: 4, e: 3, o: 3, u: 1};
*/
function vowelCount(str) {
    let object = {};
    let arr = [...str];
    arr.forEach((strEle) => {
        let ele = strEle.toLowerCase()
        if (ele === 'a' || ele === 'e' || ele === 'i' || ele === 'o' || ele === 'u') {
            if (object.hasOwnProperty(ele)) {
                object[ele] += 1;
            } else {
                object[ele] = 1;
            }
        }
    })
    return object;
}
console.log("----------------------------vowelCount---------------------------");
console.log(vowelCount('Elie'));
console.log(vowelCount('Tim'));
console.log(vowelCount('Matt'));
console.log(vowelCount('hmmm'));
console.log(vowelCount('I Am awesome and so are you'));

/*
Write a function called doubleValuesWithMap which accepts an array and returns a new array with all the values in the array passed to the function doubled

Examples:
    doubleValuesWithMap([1,2,3]) // [2,4,6]
    doubleValuesWithMap([1,-2,-3]) // [2,-4,-6]
*/

function doubleValuesWithMap(arr) {
    return arr.map((arrEle) => arrEle * 2);
}
console.log("----------------------------doubleValuesWithMap---------------------------");
console.log(doubleValuesWithMap([1, 2, 3]));
console.log(doubleValuesWithMap([1, -2, -3]));

/*
Write a function called valTimesIndex which accepts an array and returns a new array with each value multiplied by the index it is currently at in the array.

Examples:
    valTimesIndex([1,2,3]) // [0,2,6]
    valTimesIndex([1,-2,-3]) // [0,-2,-6]
*/

function valTimesIndex(arr) {
    return arr.map((arrEle, index) => arrEle * index);
}
console.log("----------------------------valTimesIndex---------------------------");
console.log(valTimesIndex([1, 2, 3]));
console.log(valTimesIndex([1, -2, -3]));

/*
Write a function called extractKey which accepts an array of objects and some key and returns a new array with the value of that key in each object.

Examples:
    extractKey([{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}, {name: 'Colt'}], 'name') // ['Elie', 'Tim', 'Matt', 'Colt']
*/

function extractKey(arr, key) {
    return arr.map((arrEle) => arrEle[key]);
}
console.log("----------------------------extractKey---------------------------");
console.log(extractKey([{ name: 'Elie' }, { name: 'Tim' }, { name: 'Matt' }, { name: 'Colt' }], 'name'))
/*
Write a function called extractFullName which accepts an array of objects and returns a new array with the value of the key with a name of "first" and the value of a key with the name of  "last" in each object, concatenated together with a space. 

Examples:
    extractFullName([{first: 'Elie', last:"Schoppik"}, {first: 'Tim', last:"Garcia"}, {first: 'Matt', last:"Lane"}, {first: 'Colt', last:"Steele"}]) // ['Elie Schoppik', 'Tim Garcia', 'Matt Lane', 'Colt Steele']
*/

function extractFullName(arr) {
    return arr.map((arrEle) => arrEle.first + " " + arrEle.last);
}
console.log("----------------------------extractFullName---------------------------");
console.log(extractFullName([{ first: 'Elie', last: "Schoppik" }, { first: 'Tim', last: "Garcia" }, { first: 'Matt', last: "Lane" }, { first: 'Colt', last: "Steele" }]));
/*
Write a function called filterByValue which accepts an array of objects and a key and returns a new array with all the objects that contain that key.

Examples:
    filterByValue([{first: 'Elie', last:"Schoppik"}, {first: 'Tim', last:"Garcia", isCatOwner: true}, {first: 'Matt', last:"Lane"}, {first: 'Colt', last:"Steele", isCatOwner: true}], 'isCatOwner') // [{first: 'Tim', last:"Garcia", isCatOwner: true}, {first: 'Colt', last:"Steele", isCatOwner: true}]
*/

function filterByValue(arr, key) {
    return arr.filter((arrEle) => arrEle.isCatOwner === true);
}
console.log("----------------------------filterByValue---------------------------");
console.log(filterByValue([{ first: 'Elie', last: "Schoppik" }, { first: 'Tim', last: "Garcia", isCatOwner: true }, { first: 'Matt', last: "Lane" }, { first: 'Colt', last: "Steele", isCatOwner: true }], 'isCatOwner'));
/*
Write a function called find which accepts an array and a value and returns the first element in the array that has the same value as the second parameter or undefined if the value is not found in the array.

Examples:
    find([1,2,3,4,5], 3) // 3
    find([1,2,3,4,5], 10) // undefined
*/

function find(arr, searchValue) {
    return arr.find((arrEle) => arrEle === searchValue);
}
console.log("----------------------------find---------------------------");
console.log(find([1, 2, 3, 4, 5], 3));
console.log(find([1, 2, 3, 4, 5], 10));

/*
Write a function called findInObj which accepts an array of objects, a key, and some value to search for and returns the first found value in the array.

Examples:
    findInObj([{first: 'Elie', last:"Schoppik"}, {first: 'Tim', last:"Garcia", isCatOwner: true}, {first: 'Matt', last:"Lane"}, {first: 'Colt', last:"Steele", isCatOwner: true}], 'isCatOwner',true) // {first: 'Tim', last:"Garcia", isCatOwner: true}
*/

function findInObj(arr, key, searchValue) {
    return arr.find((arrEle) => arrEle[key] === searchValue);
}
console.log("----------------------------findInObj---------------------------");
console.log(findInObj([{ first: 'Elie', last: "Schoppik" }, { first: 'Tim', last: "Garcia", isCatOwner: true }, { first: 'Matt', last: "Lane" }, { first: 'Colt', last: "Steele", isCatOwner: true }], 'isCatOwner', true));
/*
Write a function called removeVowels which accepts a string and returns a new string with all of the vowels (both uppercased and lowercased) removed. Every character in the new string should be lowercased.

Examples:
    removeVowels('Elie') // ('l')
    removeVowels('TIM') // ('tm')
    removeVowels('ZZZZZZ') // ('zzzzzz')
*/

function removeVowels(str) {
    let arr = [...str];
    vowelStr = 'aeiou'
    let newStr = "";
    arr.forEach((strEle) => {
        let ele = strEle.toLowerCase()
        if (!vowelStr.includes(ele)) {
            newStr += ele;
        }
        return newStr
    })
    return newStr
}
console.log("----------------------------removeVowels---------------------------");
console.log(removeVowels('Elie'));
console.log(removeVowels('TIM'));
console.log(removeVowels('ZZZZZZ'));
/*
Write a function called doubleOddNumbers which accepts an array and returns a new array with all of the odd numbers doubled (HINT - you can use map and filter to double and then filter the odd numbers).

Examples:
    doubleOddNumbers([1,2,3,4,5]) // [2,6,10]
    doubleOddNumbers([4,4,4,4,4]) // []
*/

// function doubleOddNumbers(arr) {
//     return arr.reduce((newArr, arrEle) => {
//         if (arrEle % 2 == 1) {
//             newArr.push(arrEle * 2);
//         }
//         return newArr
//     }, [])
// }
function doubleOddNumbers(arr) {
    return arr.filter((arrEle) => arrEle %2 !== 0)
        .map((arrEle) => arrEle * 2);

}
console.log("----------------------------doubleOddNumbers---------------------------");
console.log(doubleOddNumbers([1, 2, 3, 4, 5]));
console.log(doubleOddNumbers([4, 4, 4, 4, 4]));
// ============================================= Find, FindIndex ==================================================================

/* 
Write a function called `findUserByUsername` which accepts an array of objects, each with a key of username, and a string. The function should return the first object with the key of username that matches the string passed to the function. If the object is not found, return undefined. 

const users = [
    {username: 'mlewis'},
    {username: 'akagen'},
    {username: 'msmith'}
];

findUserByUsername(users, 'mlewis') // {username: 'mlewis'}
findUserByUsername(users, 'taco') // undefined
*/

function findUserByUsername(usersArray, username) {
    return usersArray.find((user) => Object.values(user) == username);
}
const users = [
    { username: 'mlewis' },
    { username: 'akagen' },
    { username: 'msmith' }
];
console.log("----------------------------findUserByUsername---------------------------");
console.log(findUserByUsername(users, 'mlewis'));
console.log(findUserByUsername(users, 'taco'));
/*
Write a function called `removeUser` which accepts an array of objects, each with a key of username, and a string. The function should remove the object from the array. If the object is not found, return undefined. 

const users = [
    {username: 'mlewis'},
    {username: 'akagen'},
    {username: 'msmith'}
];

removeUser(users, 'akagen') // {username: 'akagen'}
removeUser(users, 'akagen') // undefined
*/

function removeUser(usersArray, username) {
    result = usersArray.findIndex((user) => (Object.values(user) == username));
    let obj = { ...usersArray[result] };
    usersArray.splice(result);
    return obj;
}
const users1 = [
    { username: 'mlewis' },
    { username: 'akagen' },
    { username: 'msmith' }
];
console.log("----------------------------removeUser---------------------------");
console.log(removeUser(users1, 'akagen'));
console.log(removeUser(users1, 'akagen'));

// ============================================= Reduce ==================================================================

/*
Write a function called extractValue which accepts an array of objects and a key and returns a new array with the value of each object at the key.

Examples:
    const arr = [{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}, {name: 'Colt'}]
    extractValue(arr,'name') // ['Elie', 'Tim', 'Matt', 'Colt']
*/

function extractValue(arr, key) {
    return arr.reduce((newArr, data) => {
        newArr.push(data[key])
        return newArr;
    }, [])
}

const arr = [{ name: 'Elie' }, { name: 'Tim' }, { name: 'Matt' }, { name: 'Colt' }];
console.log("----------------------------extractValue---------------------------");
console.log(extractValue(arr, 'name'));

/*
Write a function called vowelCount which accepts a string and returns an object with the keys as the vowel and the values as the number of times the vowel appears in the string. This function should be case insensitive so a lowercase letter and uppercase letter should count

Examples:
    vowelCount('Elie') // {e:2,i:1};
    vowelCount('Tim') // {i:1};
    vowelCount('Matt') // {a:1})
    vowelCount('hmmm') // {};
    vowelCount('I Am awesome and so are you') // {i: 1, a: 4, e: 3, o: 3, u: 1};
*/

function vowelCount(str) {
    let arr = [...str];
    return arr.reduce((count, arrEle) => {
        let vowel = "aeiou";
        arrEle = arrEle.toLowerCase();
        if (vowel.includes(arrEle)) {
            if (count[arrEle]) {
                count[arrEle] += 1;
            } else {
                count[arrEle] = 1;
            }
        }
        return count;
    }, {});
}


console.log("----------------------------vowelCount---------------------------");
console.log(vowelCount("Elie"));
console.log(vowelCount("Tim"));
console.log(vowelCount("hmmm"));
console.log(vowelCount("I Am awesome and so are you"));

/*
Write a function called addKeyAndValue which accepts an array of objects and returns the array of objects passed to it with each object now including the key and value passed to the function.

Examples:
    const arr = [{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}, {name: 'Colt'}];
    
    addKeyAndValue(arr, 'title', 'Instructor') // 
    [
        {title: 'Instructor', name: 'Elie'}, 
        {title: 'Instructor', name: 'Tim'}, 
        {title: 'Instructor', name: 'Matt'}, 
        {title: 'Instructor', name: 'Colt'}
    ]
*/

function addKeyAndValue(arr, key, value) {
    let keyValue = arr.reduce((newArr, arrEle) => {
        arrEle[key] = value;
        newArr.push(arrEle);
        return newArr;
    }, []);
    return keyValue;
}
const arr1 = [{ name: 'Elie' }, { name: 'Tim' }, { name: 'Matt' }, { name: 'Colt' }];
console.log("----------------------------addKeyAndValue---------------------------");
console.log(addKeyAndValue(arr1, "title", "Instructor"));

/*
Write a function called partition which accepts an array and a callback and returns an array with two arrays inside of it. 
The partition function should run the callback function on each value in the array and 
if the result of the callback function at that specific value is true, the value should be placed in the first subarray. 
If the result of the callback function at that specific value is false, the value should be placed in the second subarray. 

Examples:
    
    function isEven(val){
        return val % 2 === 0;
    }
    
    const arr = [1,2,3,4,5,6,7,8];
    
    partition(arr, isEven) // [[2,4,6,8], [1,3,5,7]];
    
    function isLongerThanThreeCharacters(val){
        return val.length > 3;
    }
    
    const names = ['Elie', 'Colt', 'Tim', 'Matt'];
    
    partition(names, isLongerThanThreeCharacters) // [['Elie', 'Colt', 'Matt'], ['Tim']]
*/

function isEven(val){
    return val % 2 === 0;
}

function partition(arr2, callback) { 
    return arr2.reduce((newArr,ele) => {
        if (callback(ele)){
            newArr[0].push(ele)
        } else {
            newArr[1].push(ele)
        }
        return newArr
    },[[],[]])
}

const arr2 = [1,2,3,4,5,6,7,8];
console.log("----------------------------partition---------------------------");
console.log(partition(arr2, isEven));


